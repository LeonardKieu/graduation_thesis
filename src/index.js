import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import HomePage from './pages/home_page';
import 'bootstrap/dist/css/bootstrap.min.css';


ReactDOM.render(
    <HomePage/>, 
  document.getElementById('root')
);


